package com.vanya.noosphereiphtest;

import android.content.Context;

import java.io.File;

/**
 * Created by vanya on 01.06.15.
 */
public class FileCache {

    private File cacheDir;

    public FileCache(Context context) {
        cacheDir = context.getCacheDir();
    }

    public File getFile(String url) {
        String filename = String.valueOf(url.hashCode());

        File f = new File(cacheDir, filename);
        return f;

    }

    public void clear() {
        File[] files = cacheDir.listFiles();
        if (files == null)
            return;
        for (File f : files)
            f.delete();
    }
}

