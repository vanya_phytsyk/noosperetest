package com.vanya.noosphereiphtest;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.CharacterPickerDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKSdkListener;
import com.vk.sdk.VKUIHelper;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.methods.VKApiUsers;
import com.vk.sdk.api.model.VKApiUser;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKUsersArray;

import java.lang.reflect.Type;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    private static final String APP_ID = "4938027";
    private ListView lvFriends;
    FriendsAdapter friendsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        VKUIHelper.onCreate(this);
        setContentView(R.layout.activity_main);
        VKSdk.initialize(vkSdkListener, APP_ID);

        VKSdk.authorize(VKScope.FRIENDS, VKScope.PHOTOS);

        lvFriends = (ListView) findViewById(R.id.lvFriends);

        lvFriends.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, UserPhotosActivity.class);
                intent.putExtra(UserPhotosActivity.USER_ID, id);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        VKUIHelper.onResume(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        VKUIHelper.onActivityResult(this, requestCode, resultCode, data);
    }

    private VKSdkListener vkSdkListener = new VKSdkListener() {
        @Override
        public void onCaptchaError(VKError vkError) {
            //something
        }

        @Override
        public void onTokenExpired(VKAccessToken vkAccessToken) {
            //something
        }

        @Override
        public void onAccessDenied(VKError vkError) {
            //something
        }

        @Override
        public void onReceiveNewToken(VKAccessToken newToken) {
            super.onReceiveNewToken(newToken);
            VKRequest request = VKApi.friends().get(VKParameters.from(VKApiConst.FIELDS, "id,first_name,last_name,photo_100"));
            request.executeWithListener(new VKRequest.VKRequestListener() {
                @Override
                public void onComplete(VKResponse response) {
                    VKUsersArray users = (VKUsersArray) response.parsedModel;
                    if (friendsAdapter == null) {
                        friendsAdapter = new FriendsAdapter(users);
                    } else {
                        friendsAdapter.setUsers(users);
                    }
                    lvFriends.setAdapter(friendsAdapter);
                }
            });
        }
    };

    private class FriendsAdapter extends BaseAdapter {

        private VKUsersArray users;
        private ImageLoader imageLoader = new ImageLoader(MainActivity.this);

        FriendsAdapter(VKUsersArray users) {
            this.users = users;
        }

        void setUsers(VKUsersArray users) {
            this.users = users;
        }

        @Override
        public int getCount() {
            return users.size();
        }

        @Override
        public Object getItem(int position) {
            return users.get(position);
        }

        @Override
        public long getItemId(int position) {
            return ((VKApiUserFull) getItem(position)).getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = View.inflate(MainActivity.this, R.layout.itm_usr, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            VKApiUserFull user = (VKApiUserFull) getItem(position);
            holder.tvUserName.setText(user.first_name + " " + user.last_name);
            if (!TextUtils.isEmpty(user.photo_100)) {
                imageLoader.displayImage(user.photo_100, holder.imUserAvatar);
            }

            return convertView;
        }

        private class ViewHolder {
            ImageView imUserAvatar;
            TextView tvUserName;

            ViewHolder(View view) {
                imUserAvatar = (ImageView) view.findViewById(R.id.imUserAvatar);
                tvUserName = (TextView) view.findViewById(R.id.tvUserName);
            }
        }
    }
}
