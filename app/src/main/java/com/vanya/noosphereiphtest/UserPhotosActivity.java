package com.vanya.noosphereiphtest;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiModel;
import com.vk.sdk.api.model.VKApiPhoto;
import com.vk.sdk.api.model.VKPhotoArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

/**
 * Created by vanya on 01.06.15.
 */
public class UserPhotosActivity extends Activity {

    public static final String USER_ID = "userId";
    private ListView lvPhotos;
    private PhotoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_photos);
        lvPhotos = (ListView) findViewById(R.id.lvPhotos);

        long userId = getIntent().getLongExtra(USER_ID, -1);
        VKRequest request = new VKRequest("photos.get", VKParameters.from(VKApiConst.OWNER_ID, userId, VKApiConst.ALBUM_ID, "profile"));
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                Log.d("sdf", response.responseString);
                try {
                    JSONArray jsonArray = response.json.getJSONObject("response").getJSONArray("items");
                    Gson gson = new Gson();
                    Type photoListType = new TypeToken<List<VKApiPhoto>>() {
                    }.getType();
                    List<VKApiPhoto> photoList = gson.fromJson(jsonArray.toString(), photoListType);
                    adapter = new PhotoAdapter(photoList);
                    lvPhotos.setAdapter(adapter);
                } catch (JSONException e) {
                    Log.e("photos", e.getMessage());
                }

            }

        });
    }

    private class PhotoAdapter extends BaseAdapter {
        private List<VKApiPhoto> photos;
        private ImageLoader imageLoader = new ImageLoader(UserPhotosActivity.this);

        PhotoAdapter(List<VKApiPhoto> photos) {
            this.photos = photos;
        }

        @Override
        public int getCount() {
            return photos.size();
        }

        @Override
        public Object getItem(int position) {
            return photos.get(position);
        }

        @Override
        public long getItemId(int position) {
            return photos.get(position).getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = View.inflate(UserPhotosActivity.this, R.layout.itm_photo, null);

            }
            VKApiPhoto photo = (VKApiPhoto) getItem(position);
            imageLoader.displayImage(photo.photo_2560, (ImageView) convertView);

            return convertView;
        }

    }

}
